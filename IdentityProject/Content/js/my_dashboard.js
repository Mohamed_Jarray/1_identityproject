﻿
// custom delete action
function custom_delete_action(deleteButtonPressed, oTableName, data, username, actionUrl, confMessage, btn_yes, btn_no) {
    bootbox.confirm({
        buttons: {
            confirm: {
                label: btn_yes,
                className: 'btn btn-info'
            },
            cancel: {
                label: btn_no,
                className: 'btn btn-danger'
            }
        },
        message : confMessage,
        callback :function (result) {
            if (result) {
                jQuery.ajax({
                    type: "POST",
                    url: actionUrl,
                    data: JSON.stringify({ id: data }),
                    contentType: "application/json",
                    dataType: "json",
                    async: true,
                    success: function (data) {
                        var msg = data.split('|');
                        if (msg[0] === "1") {
                            var oTable = jQuery(oTableName).DataTable();
                            var row = jQuery(deleteButtonPressed).closest("tr").get(0);
                            oTable.row(row).remove().draw(false);

                            jQuery.msgGrowl({
                                type: 'success',
                                text: msg[1]
                            });
                        } else {
                            jQuery.msgGrowl({
                                type: 'error',
                                text: msg[1]
                            });
                        }
                    },
                    error: function (obj, status, error) {
                        jQuery.msgGrowl({
                            type: 'error',
                            title: 'معطيات',
                            text: 'لم تتم العملية بنجاح! حاول مرة أخرى...'
                        });
                    }
                });
            }
        }
    });

}

function custom_activate_deactivate(deleteButtonPressed, oTableName, data, username, actionUrl, confMessage, btn_yes, btn_no) {
    bootbox.confirm({
        buttons: {
            confirm: {
                label: btn_yes,
                className: 'btn btn-info'
            },
            cancel: {
                label: btn_no,
                className: 'btn btn-danger'
            }
        },
        message: confMessage,
        callback: function (result) {
            if (result) {
                jQuery.ajax({
                    type: "POST",
                    url: actionUrl,
                    data: JSON.stringify({ id: data }),
                    contentType: "application/json",
                    dataType: "json",
                    async: true,
                    success: function (data) {
                        var msg = data.split('|');
                        if (msg[0] === "1") {
                            var oTable = jQuery(oTableName).DataTable();
                            var row = jQuery(deleteButtonPressed).closest("tr").get(0);
                            oTable.row(row).remove().draw(false);

                            jQuery.msgGrowl({
                                type: 'success',
                                text: msg[1]
                            });
                        } else {
                            jQuery.msgGrowl({
                                type: 'error',
                                text: msg[1]
                            });
                        }
                    },
                    error: function (obj, status, error) {
                        jQuery.msgGrowl({
                            type: 'error',
                            title: 'معطيات',
                            text: 'لم تتم العملية بنجاح! حاول مرة أخرى...'
                        });
                    }
                });
            }
        }
    });

}

function putHTML(content) {
    if (content === null || content === '') return '&ndash;';
    else return content;
}

function dismissImage() {


    // delete image physically from server
    //jQuery.ajax({
    //    type: "POST",
    //    url: "../../Award/DeleteImage",
    //    data: '{ "photoName":"' + jQuery('#PhotoName').val() + '"}',
    //    contentType: "application/json",
    //    success: function (data) {
    //        jQuery(".dataTables_processing").css('visibility', 'hidden');
    //        window.location.reload();
    //    }
    //});
    var preview = jQuery('div.thumbnail');
    preview.html('<img src="../../../Content/img/image2x.png" style="width:30px !important;height:30px !important" />')
    // jQuery('div.thumbnail > img').attr('src', '');
    // empty hidden input PhototName
    jQuery('#PhotoName').attr('value', '');
    jQuery("#IsPhotoDelete").val("1");
}

function dismissFile() {


    // delete image physically from server
    //jQuery.ajax({
    //    type: "POST",
    //    url: "../../Award/DeleteImage",
    //    data: '{ "photoName":"' + jQuery('#PhotoName').val() + '"}',
    //    contentType: "application/json",
    //    success: function (data) {
    //        jQuery(".dataTables_processing").css('visibility', 'hidden');
    //        window.location.reload();
    //    }
    //});

    jQuery('span.fileupload-name').text("");
    // empty hidden input PhototName
    jQuery('#PdfName').attr('value', '');
}


function custom_confirm_cancel_order(deleteButtonPressed, oTableName, data, username, actionUrl, confMessage, btn_yes, btn_no, state) {
     bootbox.confirm({
    buttons: {
    confirm: {
    label: btn_yes,
    className: 'btn btn-info'
},
cancel: {
        label: btn_no,
        className: 'btn btn-danger'
}
},
message: confMessage,
    callback: function (result) {
        if (result) {
            jQuery.ajax({
                type: "POST",
                url: actionUrl,
                data: JSON.stringify({ orderId: data, state: state }),
                contentType: "application/json",
                dataType: "json",
                async: true,
                success: function (data) {
                    var msg = data.split('|');
                    if (msg[0] === "1") {
                        var oTable = jQuery(oTableName).DataTable();
                        var row = jQuery(deleteButtonPressed).closest("tr").get(0);
                        oTable.row(row).remove().draw(false);

                        jQuery.msgGrowl({
                            type: 'success',
                            text: msg[1]
                        });
                    } else {
                        jQuery.msgGrowl({
                            type: 'error',
                            text: msg[1]
                        });
                    }
                },
                error: function (obj, status, error) {
                    jQuery.msgGrowl({
                        type: 'error',
                        title: 'معطيات',
                        text: 'لم تتم العملية بنجاح! حاول مرة أخرى...'
                    });
                }
            });
        }
    }
});
}

function showEditImage(imageURL, containerId) {
    if (imageURL === '') {
        jQuery(containerId + ' div.thumbnail > img').attr('src', '@Url.Content("~/Resources/img/image2x.png")');
    }
    else {
        var preview = jQuery(containerId + ' div.thumbnail');
        preview.html('<img src="' + imageURL + '" ' + (preview.css('max-height') !== 'none' ? 'style="max-height: ' + preview.css('max-height') + ';"' : '') + ' />')
        jQuery('[data-provides="fileupload"]').addClass('fileupload-exists').removeClass('fileupload-new')
    }
}
