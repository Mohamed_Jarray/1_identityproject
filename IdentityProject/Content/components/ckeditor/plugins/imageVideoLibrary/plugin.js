﻿CKEDITOR.plugins.add('imageVideoLibrary',
{
    init: function (editor) {
        var pluginName = 'imageVideoLibrary';
        editor.ui.addButton('imageVideoLibrary',
            {
                label: 'أضف صورة',
                command: 'OpenPhotoPopUp',
                icon: CKEDITOR.plugins.getPath('imageVideoLibrary') + 'image_add.png'

            });
        var cmd = editor.addCommand('OpenPhotoPopUp', { exec: showMyDialog });
    }
});
function showMyDialog(e) {
    if(CKEDITOR.currentInstance.config.language == 'ar')
        jQuery("#dialog_ar").modal("show");
    else
        jQuery("#dialog_en").modal("show");
    
}


