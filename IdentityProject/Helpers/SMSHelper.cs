﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Xml.Linq;


namespace IdentityProject.Helpers
{
    public class SMSHelper
    {
        public static int Hams_SendSMS(string msg, string numbers)
        {
            string baseUri = "http://www.hmssms.com/api/sendsms.php?username={0}&password={1}&message={2}&numbers={3}&sender={4}&unicode=e&Rmduplicated=1&return=xml";
            string requestUri = string.Format(baseUri, ConfigurationManager.AppSettings["SMSUsername"], ConfigurationManager.AppSettings["SMSPassword"], msg, numbers, ConfigurationManager.AppSettings["SMSUsername"]);

            int success = -1;
            string errorText = null;
            using (WebClient wc = new WebClient())
            {
                wc.Encoding = System.Text.Encoding.UTF8;
                string response = wc.DownloadString(requestUri);
                var xmlElm = XElement.Parse(response);
                var status = (from elm in xmlElm.Descendants() where elm.Name == "Code" select elm).FirstOrDefault();
                if (status.Value == "100")
                    success = 1;
                else
                {
                    success = int.Parse(status.Value);
                    errorText = (from elm in xmlElm.Descendants() where elm.Name == "MessageIs" select elm).FirstOrDefault().Value;
                }

                //TribeMozainyEntities db = new TribeMozainyEntities();

                //db.spPushErrorAdd(errorText, success);

                return success;
            }
        }

    }
}