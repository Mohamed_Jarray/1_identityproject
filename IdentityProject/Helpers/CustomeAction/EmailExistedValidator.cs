﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using IdentityProject.Models;
using IdentityProject.Resources;

namespace IdentityProject.Helpers
{
    public class EmailExistedAttribute : ValidationAttribute
    {
        private IdentityProjectEntities db = new IdentityProjectEntities();
        // annotation to validate email unicity: email must be in table 'UserProfile'
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var containerType = validationContext.ObjectInstance.GetType();
            var field = containerType.GetProperty("UserId");
            string userId= field.GetValue(validationContext.ObjectInstance, null).ToString();
            string email = (string)value;
            bool isValid = (db.AspNetUsers.Where(i => i.Email == email && i.Id != userId && email != null && email != string.Empty).Count() == 0);

            if (isValid)
                return ValidationResult.Success;
            else
                return new ValidationResult(Resource.EmailUsed);
        }
    }
}