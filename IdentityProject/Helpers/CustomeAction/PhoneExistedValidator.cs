﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using IdentityProject.Models;
using IdentityProject.Resources;

namespace IdentityProject.Helpers
{
    public class PhoneExistedAttribute : ValidationAttribute
    {
        private IdentityProjectEntities db = new IdentityProjectEntities();
        // annotation to validate phone unicity: phone must be in table 'UserProfile'
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var containerType = validationContext.ObjectInstance.GetType();
            var field = containerType.GetProperty("UserId");
            string userId = field.GetValue(validationContext.ObjectInstance, null).ToString();
            string phone = (string)value;
            bool isValid = (db.AspNetUsers.Where(i => i.PhoneNumber == phone && i.Id != userId && phone != null && phone != string.Empty).Count() == 0);

            if (isValid)
                return ValidationResult.Success;
            else
                return new ValidationResult(Resource.PhoneUsed);
        }
    }
}