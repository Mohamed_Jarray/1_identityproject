﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;

namespace IdentityProject.Helpers
{
    public static class ResourceHelper
    {
        public static string MyResource<T>(this HtmlHelper html, object key)
        {
            return new ResourceManager(typeof(T)).GetString(key.ToString());
        }

        public static string MyResource<T>(object key)
        {
            return new ResourceManager(typeof(T)).GetString(key.ToString());
        }

        public static string addAl(string resource)
        {
            if (CultureHelper.IsRighToLeft())
                return "ال" + resource;
            else
                return resource;
        }
    }
}