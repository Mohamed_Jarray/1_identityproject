﻿using System;
using System.Web;


namespace IdentityProject.Helpers
{
    public class PathHelper
    {
        public static string GetBasicUrl()
        {
            HttpRequest rquest = System.Web.HttpContext.Current.Request;

            string link = rquest.Url.Scheme + System.Uri.SchemeDelimiter + rquest.Url.Host + (rquest.Url.IsDefaultPort ? "" : ":" + rquest.Url.Port);
            link += rquest.ApplicationPath;
            if (link.Substring(link.Length - 1) == "/")
                link = link.Substring(0, link.Length - 1);
            return link;
        }

    }
}