﻿using System;
using System.Net;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Configuration;


namespace IdentityProject.Helpers
{
    public class GMapHelper
    {
        public static string RetrieveFormatedAddress(string lat, string lng)
        {
            string baseUri = "http://maps.googleapis.com/maps/api/geocode/xml?latlng={0},{1}&sensor=false&language=ar";
            string requestUri = string.Format(baseUri, lat.Replace(",", "."), lng.Replace(",", "."));

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = System.Text.Encoding.UTF8;
                string result = wc.DownloadString(requestUri);
                var xmlElm = XElement.Parse(result);
                var status = (from elm in xmlElm.Descendants() where elm.Name == "status" select elm).FirstOrDefault();
                if (status.Value.ToLower() == "ok")
                {
                    var res = (from elm in xmlElm.Descendants() where elm.Name == "result" select elm).FirstOrDefault();
                    var adr = (from elm in res.Descendants() where elm.Name == "formatted_address" select elm).FirstOrDefault();
                    return adr.Value;
                }
                else
                {
                    return "";
                }
            }

        }

        public static string RetrieveFormatedAddress_English(string lat, string lng)
        {
            string baseUri = "http://maps.googleapis.com/maps/api/geocode/xml?latlng={0},{1}&sensor=false";
            string requestUri = string.Format(baseUri, lat.Replace(",", "."), lng.Replace(",", "."));

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = System.Text.Encoding.UTF8;
                string result = wc.DownloadString(requestUri);
                var xmlElm = XElement.Parse(result);
                var status = (from elm in xmlElm.Descendants() where elm.Name == "status" select elm).FirstOrDefault();
                if (status.Value.ToLower() == "ok")
                {
                    var res = (from elm in xmlElm.Descendants() where elm.Name == "result" select elm).FirstOrDefault();
                    var adr = (from elm in res.Descendants() where elm.Name == "formatted_address" select elm).FirstOrDefault();
                    return adr.Value;
                }
                else
                {
                    return "";
                }
            }

        }

        public static string[] RetrieveAddress_component(string lat, string lng)
        {
            string[] address_component = new string[6];
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load("http://maps.googleapis.com/maps/api/geocode/xml?latlng=" + lat + "," + lng + "&sensor=false&language=ar");
                XmlNode element = doc.SelectSingleNode("//GeocodeResponse/status");
                if (element.InnerText == "ZERO_RESULTS")
                {
                    address_component[5] = "ZERO_RESULTS";
                    return address_component;
                }
                else
                {
                    element = doc.SelectSingleNode("//GeocodeResponse/result/formatted_address");

                    string longname = "";
                    string shortname = "";
                    string typename = "";

                    XmlNodeList xnList = doc.SelectNodes("//GeocodeResponse/result[2]/address_component");
                    foreach (XmlNode xn in xnList)
                    {
                        try
                        {
                            longname = xn["long_name"].InnerText;
                            shortname = xn["short_name"].InnerText;
                            if (xn["type"] != null)
                                typename = xn["type"].InnerText;

                            switch (typename)
                            {
                                //Add whatever you are looking for below                            
                                case "locality":
                                    {
                                        if (String.IsNullOrEmpty(address_component[0]))
                                            address_component[0] = longname;
                                        //address_component[0] = shortname;
                                        break;
                                    }

                                case "administrative_area_level_1":
                                    {
                                        if (String.IsNullOrEmpty(address_component[1]))
                                            address_component[1] = longname;
                                        break;
                                    }

                                case "country":
                                    {
                                        if (String.IsNullOrEmpty(address_component[2]))
                                            address_component[2] = longname;
                                        break;
                                    }

                                case "sublocality_level_1":
                                    {
                                        if (String.IsNullOrEmpty(address_component[3]))
                                            address_component[3] = longname;
                                        //Address_locality = shortname; //Om Longname visar sig innehålla konstigheter kan man använda shortname istället
                                        break;
                                    }


                                default: break;
                            }
                        }
                        catch (Exception)
                        {
                            //Node missing either, longname, shortname or typename
                            //return address_component;
                        }
                    }

                }

                address_component[4] = address_component[3] + ',' + address_component[0] + ',' + address_component[1] + ',' + address_component[2];
                return address_component;

            }
            catch (Exception ex)
            {
                address_component[3] = ex.Message;
                return address_component;
            }
        }

        public static string[] RetrieveAddress_component_English(string lat, string lng)
        {
            string[] address_component = new string[6];
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load("http://maps.googleapis.com/maps/api/geocode/xml?latlng=" + lat + "," + lng + "&sensor=false");
                XmlNode element = doc.SelectSingleNode("//GeocodeResponse/status");
                if (element.InnerText == "ZERO_RESULTS")
                {
                    address_component[5] = "ZERO_RESULTS";
                    return address_component;
                }
                else
                {
                    string longname = "";
                    string shortname = "";
                    string typename = "";

                    XmlNodeList xnList = doc.SelectNodes("//GeocodeResponse/result/address_component");
                    foreach (XmlNode xn in xnList)
                    {
                        try
                        {
                            longname = xn["long_name"].InnerText;
                            shortname = xn["short_name"].InnerText;
                            if (xn["type"] != null)
                                typename = xn["type"].InnerText;

                            switch (typename)
                            {
                                //Add whatever you are looking for below                            
                                case "locality":
                                    {
                                        if (String.IsNullOrEmpty(address_component[0]))
                                            address_component[0] = longname;
                                        //address_component[0] = shortname;
                                        break;
                                    }

                                case "administrative_area_level_1":
                                    {
                                        if (String.IsNullOrEmpty(address_component[1]))
                                            address_component[1] = longname;
                                        break;
                                    }

                                case "country":
                                    {
                                        if (String.IsNullOrEmpty(address_component[2]))
                                            address_component[2] = longname;
                                        break;
                                    }

                                case "sublocality_level_1":
                                    {
                                        if (String.IsNullOrEmpty(address_component[3]))
                                            address_component[3] = longname;
                                        //Address_locality = shortname; //Om Longname visar sig innehålla konstigheter kan man använda shortname istället
                                        break;
                                    }


                                default: break;
                            }
                        }
                        catch (Exception)
                        {
                            //Node missing either, longname, shortname or typename
                            //return address_component;
                        }
                    }

                }

                address_component[4] = address_component[3] + ',' + address_component[0] + ',' + address_component[1] + ',' + address_component[2];
                return address_component;

            }
            catch (Exception ex)
            {
                address_component[3] = ex.Message;
                return address_component;
            }
        }

        public static string[] RetrieveDistanceAndDuration(int language, string latitudeFrom, string longitudeFrom, string latitudeTo, string longitudeTo)
        {
            string[] result = new string[5];
            try
            {
                string baseUri = "";
                if (language == 1)
                    baseUri = "https://maps.googleapis.com/maps/api/distancematrix/xml?origins={0},{1}&destinations={2},{3}&key={4}&language=ar";
                else
                    baseUri = "https://maps.googleapis.com/maps/api/distancematrix/xml?origins={0},{1}&destinations={2},{3}&key={4}";

                string requestUri = string.Format(baseUri, latitudeFrom.Replace(",", "."), longitudeFrom.Replace(",", "."), latitudeTo.Replace(",", "."), longitudeTo.Replace(",", "."), ConfigurationManager.AppSettings["GoogleMAPApiKey"]);

                using (WebClient wc = new WebClient())
                {

                    wc.Encoding = System.Text.Encoding.UTF8;
                    string response = wc.DownloadString(requestUri);
                    var xmlElm = XElement.Parse(response);
                    var status = (from elm in xmlElm.Descendants() where elm.Name == "status" select elm).FirstOrDefault();
                    result[0] = status.Value.ToLower();
                    if (result[0] == "ok")
                    {
                        var row = (from elm in xmlElm.Descendants() where elm.Name == "row" select elm).FirstOrDefault();
                        var element = (from elm in row.Descendants() where elm.Name == "element" select elm).FirstOrDefault();
                        var statusElement = (from elm in element.Descendants() where elm.Name == "status" select elm).FirstOrDefault().Value;
                        if (!statusElement.ToLower().Equals("ok"))
                        {
                            return new string[5];
                        }



                        var duration = (from elm in element.Descendants() where elm.Name == "duration" select elm).FirstOrDefault();
                        var distance = (from elm in element.Descendants() where elm.Name == "distance" select elm).FirstOrDefault();

                        result[1] = (from elm in duration.Descendants() where elm.Name == "value" select elm).FirstOrDefault().Value;
                        result[2] = (from elm in duration.Descendants() where elm.Name == "text" select elm).FirstOrDefault().Value;
                        // distance

                        result[3] = (from elm in distance.Descendants() where elm.Name == "value" select elm).FirstOrDefault().Value;
                        result[4] = (from elm in distance.Descendants() where elm.Name == "text" select elm).FirstOrDefault().Value;
                    }
                    return result;
                }
            }
            catch (Exception)
            {
                return new string[5];
            }
        }

    }
}