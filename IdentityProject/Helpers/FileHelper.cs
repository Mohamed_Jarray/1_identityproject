﻿using System;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Helpers;

namespace IdentityProject.Helpers
{
    public class FileHelper
    {
        public const int VIDEO_FILE_NUMBER = 1;
        public const int AUDIO_FILE_NUMBER = 2;
        public const int DOC_FILE_NUMBER = 3;
        public const int IMAGE_FILE_NUMBER = 4;

        public static void UploadBase64Image(string Img, string directory, string name, string upload_url)
        {
            using (MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(Img)))
            {
                Image image = Image.FromStream(memoryStream);
                string path = HttpRuntime.AppDomainAppPath + @"\\" + upload_url + "\\" + directory + "\\";
                image.Save(path + "\\" + name);
                CreateThumbNails(Img, path, name);
            }            
        }

        public static string UploadImageFromApi(string imageSrc, string directory, string upload_url)
        {
            string name = null;
            if (imageSrc != null && imageSrc != string.Empty)
            {
                name = GenerateFileName();
                UploadBase64Image(imageSrc, directory, name, upload_url);
            }
            return name;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="counter">counter used for multiple uplaod at once => must have different name</param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static string GenerateFileName(int counter, string extension = ".jpg")
        {
            return counter + DateTime.Now.TimeOfDay.ToString().Replace(":", string.Empty).Replace(" ", string.Empty).Replace(".", string.Empty) + extension;
        }

        public static string GenerateFileName(string extension = ".jpg")
        {
            Random rnd = new Random();
            return DateTime.Now.TimeOfDay.ToString().Replace(":", string.Empty).Replace(" ", string.Empty).Replace(".", string.Empty) + rnd.Next(100) + extension;
        }

        public static string UploadImage(HttpPostedFileBase photoName, string url)
        {
            var fileName = Path.GetFileName(photoName.FileName);
            var extension = Path.GetExtension(fileName);
            fileName = GenerateFileName(extension);
            var path = Path.Combine(HttpContext.Current.Server.MapPath(url), fileName);
            photoName.SaveAs(path);
            CreateThumbNails(photoName, url, fileName);
            return fileName;
        }

        public static void CreateThumbNails(HttpPostedFileBase photoName, string path, string name)
        {
            Image img = Image.FromStream(photoName.InputStream);
            int width_Origin = img.Width;
            int height_Origin = img.Height;
            photoName.InputStream.Seek(0, SeekOrigin.Begin);

            if (photoName != null && photoName.ContentLength > 0)
            {
                string file1X2 = "1X2" + name;
                string file1X4 = "1X4" + name;
                string file3X4 = "3X4" + name;

                string thumpMap = HttpContext.Current.Server.MapPath(path);

                ImageResizer.ResizeSettings paramResize = new ImageResizer.ResizeSettings
                {
                    Width = (width_Origin / 2),
                    Height = (height_Origin / 2),
                    Mode = ImageResizer.FitMode.Carve
                };

                ImageResizer.ImageBuilder.Current.Build(photoName, thumpMap + file1X2, paramResize);

                // ******* //
                paramResize = new ImageResizer.ResizeSettings
                {
                    Width = (width_Origin / 4),
                    Height = (height_Origin / 4),
                    Mode = ImageResizer.FitMode.Carve
                };

                ImageResizer.ImageBuilder.Current.Build(photoName, thumpMap + file1X4, paramResize);

                //*******//
                paramResize = new ImageResizer.ResizeSettings
                {
                    Width = ((width_Origin * 3) / 4),
                    Height = ((height_Origin * 3) / 4),
                    Mode = ImageResizer.FitMode.Carve
                };

                ImageResizer.ImageBuilder.Current.Build(photoName, thumpMap + file3X4, paramResize);
            }
        }

        public static void CreateThumbNails(string photoName, string path, string name)
        {
            byte[] imageBytes = Convert.FromBase64String(photoName);
            // byte[] bytes = System.IO.File.ReadAllBytes(photoName);

            var memoryStreams = new MemoryStream(imageBytes, 0, imageBytes.Length);
            var memoryStreams2 = new MemoryStream(imageBytes, 0, imageBytes.Length);
            var memoryStreams3 = new MemoryStream(imageBytes, 0, imageBytes.Length);

            Image image = Image.FromStream(memoryStreams, true);
            int width_Origin = image.Width;
            int height_Origin = image.Height;

            // photoName.InputStream.Seek(0, SeekOrigin.Begin);

            WebImage thumbnail1 = new WebImage(memoryStreams).Resize((int)(width_Origin / 2), (int)(height_Origin / 2));
            WebImage thumbnail2 = new WebImage(memoryStreams2).Resize((int)(width_Origin / 4), (int)(height_Origin / 4));
            WebImage thumbnail3 = new WebImage(memoryStreams3).Resize((int)(width_Origin * 3 / 4), (int)(height_Origin * 3 / 4));

            thumbnail1.Save(path + "1X2" + name, "jpg");
            thumbnail2.Save(path + "1X4" + name, "jpg");
            thumbnail3.Save(path + "3X4" + name, "jpg");
        }


        public static int GetFileType(string source)
        {
            source = source.ToUpperInvariant();
            if (source.EndsWith(".AVI") || source.EndsWith(".MP4") || source.EndsWith(".MPE") || source.EndsWith(".MPEG") || source.EndsWith(".MPG") || source.EndsWith(".WAV") || source.EndsWith(".3GP") || source.EndsWith(".WEBM"))
                return VIDEO_FILE_NUMBER; // video
            if (source.EndsWith(".AA3") || source.EndsWith(".MP3") || source.EndsWith(".AAX") || source.EndsWith(".AC3") || source.EndsWith(".AU") || source.EndsWith(".M4A") || source.EndsWith(".AMR") || source.EndsWith(".WAV") || source.EndsWith(".OGG") || source.EndsWith(".AAC"))
                return AUDIO_FILE_NUMBER; // audio
            else if (source.EndsWith(".DOC") || source.EndsWith(".DOCX") || source.EndsWith(".PDF") || source.EndsWith(".PPT") || source.EndsWith(".PPTX") || source.EndsWith(".3GP") || source.EndsWith(".XLS") || source.EndsWith(".XLSX") || source.EndsWith(".ZIP") || source.EndsWith(".RAR"))
                return DOC_FILE_NUMBER; // doc
            if (source.EndsWith(".PNG") || source.EndsWith(".JPG") || source.EndsWith(".BMP") || source.EndsWith(".GIF") || source.EndsWith(".JPEG"))
                return IMAGE_FILE_NUMBER; // image
            else
                return 5; // link
        }

    }
}