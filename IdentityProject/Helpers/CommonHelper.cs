﻿using IdentityProject.Resources;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.ModelBinding;
using System.Web.Security;

namespace IdentityProject.Helpers
{
    public class CommonHelper
    {
        public static string TYPE_MSG_SUCESS = "success";
        public static string TYPE_MSG_ERROR = "error";
        private static readonly string[] pnV1 = { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };
        private static readonly string[] pnV2 = { "٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩" };
        private static readonly string[] ar = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

        public static void DeleteAccount(string role, string username)
        {
            if (Roles.GetUsersInRole(role).Where(i => i == username).Count() > 0)
            {
                Roles.RemoveUserFromRole(username, role);
            }
        }

        public static string PrepareFilterString(string param)
        {
            string result = null;
            if (param.Length > 0 && !param.Equals("undefined"))
                result = param.Trim();
            return result;
        }

        public static int? PrepareFilterInt(string param)
        {
            if (param == null) return null;
            int? result = null;
            if (param.Length > 0 && !param.Equals("undefined") && int.Parse(param) != 0)
                result = int.Parse(param.Trim());
            return result;
        }

        public static string GetIsActiveString(int? isActive)
        {
            string result = "-";
            switch (isActive)
            {
                case 1: return Resource.Activated;
                case 2: return Resource.Deactivated;
                case null: return result;
            }
            return result;
        }

        public static string GetResourceByKey(string key)
        {
            System.Resources.ResourceManager rm = new System.Resources.ResourceManager("IdentityProject.Resources.Resource", typeof(CommonHelper).Assembly);
           // System.Resources.ResourceManager rm = new System.Resources.ResourceManager("IdentityProject", typeof(IdentityProject.Resources.Resource).Assembly);
            return rm.GetString(key);
        }


        public static string StripHTML(string HTMLText)
        {
            Regex reg = new Regex("<[^>]+>|&nbsp;", RegexOptions.IgnoreCase);

            return reg.Replace(HTMLText, "");
        }

        public static string GetExceptionDetails(Exception exp)
        {
            return exp.Message + "|||||||" + exp.Source + "|||||||" + exp.InnerException + "|||||||" + exp.Data.Values + "|||||||" + exp.StackTrace;
        }

        /// <summary>
        /// get the activation message to send to users
        /// </summary>
        /// <returns></returns>
        public static string GetActivationMessage(string code)
        {
            return Resource.ActivationMessage.Replace("##WEBSITENAME##", Resource.WebSiteName).Replace("##CODE##", code);
        }

        public static string ShowModelError(ModelStateDictionary modelState)
        {
            string validationError = "";
            foreach (ModelState modelValues in modelState.Values)
            {
                foreach (ModelError error in modelValues.Errors)
                {
                    validationError += error.ErrorMessage + "|||||||";
                }
            }
            return validationError;
        }

        public static string ToArabicNumber(string persianNumber)
        {
            string arabicNumber = persianNumber;
            for (int i = 0; i < 10; i++)
            {
                arabicNumber = arabicNumber.Replace(pnV1[i], ar[i]);
                arabicNumber = arabicNumber.Replace(pnV2[i], ar[i]);
            }
            return arabicNumber;
        }

    }
}