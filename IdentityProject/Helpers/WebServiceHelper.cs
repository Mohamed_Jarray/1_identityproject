﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IdentityProject.Helpers
{
    public class WebServiceHelper
    {
        public const string SECURITY_ERROR = "Error: Access security error.";
        public const string DISABLED_USER_ERROR = "Error: Disabled user.";
        public const string INCORRECT_LOGIN_PASSWORD = "Error: incorrect email and/or password.";
        public const string INCORRECT_PHONE = "Error: There is no user registred with this phone.";
        public const string BLOCKED_USER_ERROR_MESSAGE = "Error: you can't do this operation because you are blocked: your phone is not verified";
        public const string NO_ROW_AFFECTED = "Error: no row affected.";
        public const string NO_ROW_SELECTED = "no row selected.";
        public const string CANNOT_SEND_EMAIL = "Error: problem sending the email";
        public const string OTHER_Exception = "system exception";
        public const string UNSENT_MESSAGE_ERROR = "Error: unsent message.";
        public const string SUCCESS_MESSAGE = "Success.";
        public const string USER_NOT_FOUND_MESSAGE = "Error: user not found.";
        public const string EMAIL_USED_MESSAGE = "Error: There is one user registred with this email.";
        public const string PHONE_USED_MESSAGE = "Error: There is one user registred with this phone.";
        public const string PHONE_ALREADY_ACTIVATED_MESSAGE = "Error : phone is already activated";
        public const string PHONE_NOT_FOUND_MESSAGE = "Error : no one registred with this phone";
        public const string EMAIL_NOT_FOUND = "Error : no one registred with email";
        public const string CODE_INCORRECT_MESSAGE = "Error : the code is incorrect";
        public const string NO_MORE_ATTEMPS_MESSAGE = "Code Error: you complete your max allowed attemps";
        public const string VALIDATION_ERROR_MESSAGE = "validation error";
        public const string CANNOT_UPDATE_TO_STATE_MESSAGE = "Error : connot update order to this state";
        public const string WRONG_STATE_MESSAGE = "Error : wrong state";
        public const string CANNOT_UPDATE_FROM_STATE_MESSAGE = "Error : connot update order from this state";
        public const string INSUFFICIENT_SOLD_MESSAGE = "Error : insufficient sold";
        public const string NO_DRIVER_IN_QUARTER_MESSAGE = "Error : no driver registred in this quarter";
        public const string SPECIFY_LAUNDRY_MESSAGE = "Error : updating to this state requires selected laundry Id";
        public const string WRONG_EVALUATION_NUMBER_MESSAGE = "Error : wrong evaluation number";
        public const string ORDER_NOT_FOUND_MESSAGE = "Error : order not found";
        public const string CANNOT_UPDATE_CLOTHES_MESSAGE = "Error : cannot update order's clothes after state 4";
        public const string SPECIFY_CLOTHES_MESSAGE = "Error : updating to this state requires clothes selection";
        public const string ORDER_NOT_PAID_MESSAGE = "Error : order isn't paid yet";

        public const int SUCCESS_NUMBER = 1;
        public const int OTHER_EXCEPTION = -2;
        public const int EMAIL_USED_EXCEPTION = -3;
        public const int PHONE_USED_EXCEPTION = -4;
        public const int NO_ROW_SELECTED_EXCEPTION = -5;
        public const int NO_ROW_AFFECTED_EXCEPTION = -6;
        public const int PHONE_NOT_FOUND_EXCEPTION = -7;
        public const int BLOCKED_USER_ERROR_EXCEPTION = -8;
        public const int UNSENT_MESSAGE_EXCEPTION = -9;
        public const int USER_NOT_FOUND_EXCEPTION = -10;
        public const int PHONE_ALREADY_ACTIVATED_EXCEPTION = -20;
        public const int CODE_INCORRECT_EXCEPTION = -21;
        public const int NO_MORE_ATTEMPS_EXCEPTION = -22;
        public const int VALIDATION_ERROR_EXCEPTION = -30;
        public const int WRONG_STATE_EXCEPTION = -40;
        public const int CANNOT_UPDATE_FROM_STATE_EXCEPTION = -41;
        public const int CANNOT_UPDATE_TO_STATE_EXCEPTION = -42;
        public const int INSUFFICIENT_SOLD_EXCEPTION = -43;
        public const int NO_DRIVER_IN_QUARTER_EXCEPTION = -44;
        public const int SPECIFY_LAUNDRY_EXCEPTION = -45;
        public const int WRONG_EVALUATION_NUMBER_EXCEPTION = -46;
        public const int ORDER_NOT_FOUND_EXCEPTION = -47;
        public const int CANNOT_UPDATE_CLOTHES_EXCEPTION = -48;
        public const int SPECIFY_CLOTHES_EXCEPTION = -49; 
        public const int ORDER_NOT_PAID_EXCEPTION = -51;
        public static int COUPON_NOT_VALID_EXCEPTION { get { return -50; } }
        public static string COUPON_NOT_VALID_MESSAGE { get { return "Error  : coupon not valid"; } }
    }
}