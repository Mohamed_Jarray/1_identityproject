﻿using System.Collections.Generic;
using System.Web;
using IdentityProject.Resources;
using System.Web.Mvc;
using System.Text;
using System.Web.Hosting;
using IdentityProject.Helpers;

namespace IdentityProject.Helpers
{
    public class CustomHelper
    {
        public static string CustomerURL = PathHelper.GetBasicUrl() + "/Zdnn000LAUpload/__zd00__Customer/";
        public static string DriverURL = PathHelper.GetBasicUrl() + "/Zdnn000LAUpload/__zd00__Driver/";
        public static string LaundryURL = PathHelper.GetBasicUrl() + "/Zdnn000LAUpload/__zd00__Laundry/";
        public static string LaundryPath = "~/Zdnn000LAUpload/__zd00__Laundry/";
        public static string DriverPath = "~/Zdnn000LAUpload/__zd00__Driver/";

        public const string URL_UPLOAD = "Zdnn000LAUpload";
        public const string FOLDER_CUSTOMER = "__zd00__Customer";
        public const string FOLDER_DRIVER = "__zd00__Driver";
        public const string FOLDER_LAUNDRY = "__zd00__Laundry";

        public const string ZEDNEYADMIN = "زدني";
        public const string SUPERADMIN = "مدير النظام";
        public const string ADMINISTRATOR = "مدير";
        public const string CUSTOMER = "عميل";
        public const string DRIVER = "سائق";
        public const string LAUNDRY = "مغسلة";

        public static string FontPath = HostingEnvironment.MapPath("~/Content/fonts/JF-Flat-regular.ttf");

        public static List<SelectListItem> GetGenders = new List<SelectListItem> {
            new SelectListItem { Value="1",Text= Resource.Male},
            new SelectListItem { Value="2",Text= Resource.Female},
        };

        public static List<SelectListItem> GetActivationStates = new List<SelectListItem> {
            new SelectListItem { Value="1",Text= Resource.Activated},
            new SelectListItem { Value="2",Text= Resource.Deactivated},
        };

        public static List<SelectListItem> GetAuthentications = new List<SelectListItem> {
            new SelectListItem { Value="1",Text= Resource.Yes},
            new SelectListItem { Value="2",Text= Resource.No},
        };

        public static List<SelectListItem> GetEvaluations = new List<SelectListItem> {
            new SelectListItem { Value="1",Text= "1"},
            new SelectListItem { Value="2",Text= "2"},
            new SelectListItem { Value="1",Text= "3"},
            new SelectListItem { Value="2",Text= "4"},
            new SelectListItem { Value="2",Text= "5"},
        };

        private static string ShowStar(int starNumber)
        {
            StringBuilder result = new StringBuilder();
            for (int counter = 0; counter < 5; counter++)
            {
                if (counter < starNumber)
                    result.Append("<span class='fa fa-star gold'></span>");
                else
                    result.Append("<span class='fa fa-star grey'></span>");
            }
            return HttpUtility.HtmlEncode(result);
        }

        public static string GetGenderName(int genderId)
        {
            switch (genderId)
            {
                case 1: return Resource.Male;
                case 2: return Resource.Female;
                default: return null;
            }
        }

        public static string GetActivationStateName(int state)
        {
            switch (state)
            {
                case 1: return Resource.Activated;
                case 2: return Resource.Deactivated;
                default: return null;
            }
        }

        public static IHtmlString CustomRow(string damage, string model, string device, string username, int userId, int orderId)
        {
            string result = "";
            return new HtmlString(result);
        }

    }
}