﻿using IdentityProject.Models;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;

namespace IdentityProject.Controllers
{
    public class HomeController : BaseController
    {

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public HomeController()
        {
        }

        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult _Header()
        {
            ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
            ViewBag.Username = user.UserName;
            ViewBag.Role = UserManager.GetRoles(User.Identity.GetUserId()).FirstOrDefault();
            return PartialView("~/Views/Shared/Components/Header.cshtml");
        }

        public PartialViewResult _Menu()
        {
            List<SubMenuItem> settingMenu = new List<SubMenuItem>();            
            ViewBag.SettingSubMenuItemList = settingMenu;
            return PartialView("~/Views/Shared/Components/Menu.cshtml");
        }


    }
}