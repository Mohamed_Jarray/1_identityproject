﻿
namespace IdentityProject.Models
{
    public class SubMenuItem
    {
        public string Name { get; set; }
        public string UrlAction { get; set; }
    }
}