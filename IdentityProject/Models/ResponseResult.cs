﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IdentityProject.Models
{
    public class ResponseResult<T>
    {
        public int ResultNumber { get; set; }
        public string ResultMessage { get; set; }
        public T ResultObject { get; set; }
    }
}