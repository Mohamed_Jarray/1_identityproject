﻿using System.Web.Mvc;

namespace IdentityProject.Areas.Administrators
{
    public class AdministratorsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Administrators";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Administrators_default",
                "Administrators/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "IdentityProject.Areas.Administrators.Controllers" }

            );
        }
    }
}