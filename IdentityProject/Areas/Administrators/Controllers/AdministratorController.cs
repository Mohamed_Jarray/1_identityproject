﻿using IdentityProject.Controllers;
using IdentityProject.Models;
using IdentityProject.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IdentityProject.Resources;
using System.Threading.Tasks;
using System.Net;

namespace IdentityProject.Areas.Administrators.Controllers
{
    [CustomAuthorize(Roles = CustomHelper.SUPERADMIN + "," + CustomHelper.ADMINISTRATOR)]
    public class AdministratorController : BaseController
    {
        private IdentityProjectEntities db = new IdentityProjectEntities();

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AdministratorController()
        {
        }

        public AdministratorController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administrators/Administrator
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetAjaxData()
        {
            #region Build Order by Clause And Pagination parameters
            Pagination pagination = new Pagination();
            pagination = DataTablePagination(pagination);
            #endregion

            int? count = 0;
            string name = null;
            string username = null;
            string email = null;
            string phone = null;

            name = CommonHelper.PrepareFilterString(Request["sSearch_0"]);
            username = CommonHelper.PrepareFilterString(Request["sSearch_1"]);
            email = CommonHelper.PrepareFilterString(Request["sSearch_2"]);

            List<spAdministratorGetByPage_Result> adminList = db.spAdministratorGetByPage(name, username, email, phone, pagination.SortColIndex, pagination.OrderByColumnDir, pagination.PageNum, pagination.DisplayLength).ToList();
            if (adminList.Count > 0)
            {
                count = adminList.First().TotalRows;
            }

            string connectedRole = UserManager.GetRoles(User.Identity.GetUserId()).FirstOrDefault();

            var Data = adminList.Select(d => new string[] {
                            d.Name,
                            d.UserName,
                            d.Email,
                            d.PhoneNumber,
                            d.Id.ToString() + "," + 
                            // 1 => connected role superadmin and returned role superadmin
                            // 2 => connected role superadmin and returned role simple admin
                            // 3 => connected role simple admin and returned role superadmin
                            // 4 => connected role simple admin and returned role simple admin
                            ((connectedRole.Equals(CustomHelper.SUPERADMIN))? (d.RoleName.Equals(CustomHelper.SUPERADMIN))? 1 : 2 : (d.RoleName.Equals(CustomHelper.SUPERADMIN))? 3 : 4)
            }).ToArray();
            return Json(new
            {
                aaData = Data,
                iTotalDisplayRecords = count,
                iTotalRecords = count,
                sEcho = Request.Params["sEcho"]

            }, JsonRequestBehavior.AllowGet);
        }

        // GET: /Administrator/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Administrator/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[PhoneExisted]
        //[EmailExisted]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = new ApplicationUser { UserName = model.UserName, Email = model.Email, Name = model.Name, PhoneNumber = model.Phone };
                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var roleresult = UserManager.AddToRole(user.Id, CustomHelper.ADMINISTRATOR);

                        ShowGrowlMessage(Resource.SuccessOperation, CommonHelper.TYPE_MSG_SUCESS);
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    CommonHelper.DeleteAccount(CustomHelper.ADMINISTRATOR, model.UserName);
                    ShowGrowlMessage(Resource.ErrorOperation, CommonHelper.TYPE_MSG_ERROR);
                }
            }
            return View(model);
        }

        // GET: /Administrator/Edit/5 
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = UserManager.FindById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            EditRegisterViewModel admin = new EditRegisterViewModel()
            {
                Name = user.Name,
                UserId = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                Phone = user.PhoneNumber
            };
            // administrator cannot edit superadmin
            string connectedRole = UserManager.GetRoles(User.Identity.GetUserId()).FirstOrDefault();
            if (connectedRole.Equals(CustomHelper.ADMINISTRATOR) && admin.RoleName.Equals(CustomHelper.SUPERADMIN))
                return RedirectToAction("LogOff", "Account", new { area = "" });
            return View(admin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[PhoneExisted]
        //[EmailExisted]
        public async Task<ActionResult> Edit(EditRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ApplicationUser user = UserManager.FindById(model.UserId);
                    user.Name = model.Name;
                    user.PhoneNumber = model.Phone;
                    user.Email = model.Email;
                    UserManager.Update(user);
                    if (!string.IsNullOrEmpty(model.Password))
                    {
                        string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                        var result = await UserManager.ResetPasswordAsync(user.Id, code, model.Password);
                        if (!result.Succeeded)
                            ShowGrowlMessage(Resource.ErrorOperation, CommonHelper.TYPE_MSG_ERROR);
                    }
                    ShowGrowlMessage(Resource.SuccessOperation, CommonHelper.TYPE_MSG_SUCESS);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    CommonHelper.DeleteAccount(CustomHelper.ADMINISTRATOR, model.UserName);
                    ShowGrowlMessage(Resource.ErrorOperation, CommonHelper.TYPE_MSG_ERROR);
                }
            }
            return View(model);
        }

    }
}