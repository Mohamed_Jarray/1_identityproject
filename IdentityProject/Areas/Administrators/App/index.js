﻿$("#Menu_Administrator").addClass("active");

function insialise_administrator_datatable(datatable_parameters) {
    console.log(1)
    jQuery(document).ready(function () {
        var oTable = jQuery(datatable_parameters.TableName).DataTable({
            "aoColumnDefs": [
                {
                    'aTargets': [0],
                    'bSortable': true,
                    'mRender': function (data, type, full) {
                        if (data == null)
                            return '&ndash;';
                        else
                            return '<div class="alignRight">' + data + '</div>';
                    }
                },
                {
                    'aTargets': [1],
                    'bSortable': true,
                    'mRender': function (data, type, full) {
                        if (data == null)
                            return '&ndash;';
                        else
                            return '<div class="alignRight">' + data + '</div>';
                    }
                },
                {
                    'aTargets': [2],
                    'bSortable': true,
                    'mRender': function (data, type, full) {
                        if (data == null)
                            return '&ndash;';
                        else
                            return '<div class="alignRight">' + data + '</div>';
                    }
                },
                {
                    'aTargets': [3],
                    'bSortable': true,
                    'mRender': function (data, type, full) {
                        if (data == null)
                            return '&ndash;';
                        else
                            return '<div>' + data + '</div>';
                    }
                },
                {
                    'aTargets': [4],
                    'bSortable': false,
                    'mRender': function (data, type, row) {
                       
                        var tab = data.split(',');
                        if (tab[1] === '1') {
                            return '<div>'
                                + '<a href="' + datatable_parameters.EditUrl + '/' + tab[0] + '" class="btn btn-info btn-xs edit"><i class="fa fa-pencil margin-right-5"></i>' + datatable_parameters.BtnEdit + '</a>&nbsp;'
                                + '</div>';
                        } else if (tab[1] === '2') {
                            return '<div>'
                                + '<a href="' + datatable_parameters.EditUrl + '/' + tab[0] + '" class="btn btn-info btn-xs edit"><i class="fa fa-pencil margin-right-5"></i>' + datatable_parameters.BtnEdit + '</a>&nbsp;'
                                + '<a  href="javascript:;" class="btn btn-danger btn-xs delete" onclick="delete_model(this,' + tab[0] + ');"><i class="fa fa-trash margin-right-5"></i>' + datatable_parameters.BtnDelete + '</a>'
                                + '</div>';
                        }
                        else if (tab[1] === '3') {
                            return '&ndash;';
                        }
                        else {
                            return '<div>'
                                + '<a href="' + datatable_parameters.EditUrl + '/' + tab[0] + '" class="btn btn-info btn-xs edit"><i class="fa fa-pencil margin-right-5"></i>' + datatable_parameters.BtnEdit + '</a>&nbsp;'
                                + '<a  href="javascript:;" class="btn btn-danger btn-xs delete" onclick="delete_model(this,' + tab[0] + ');"><i class="fa fa-trash margin-right-5"></i>' + datatable_parameters.BtnDelete + '</a>'
                                + '</div>';
                        }
                    }
                }
            ],

            "sAjaxSource": datatable_parameters.ActionUrl,
            "bServerSide": true,
            "bLengthChange": true,
            "iDisplayLength": 10,
            "bStateSave": true,
            "fnPreDrawCallback": function (oSettings) {
                $('.loading-container').removeClass('loading-inactive').addClass('loading-active');
            },
            "fnDrawCallback": function (oSettings) {
                $('.loading-container').removeClass('loading-active').addClass('loading-inactive');
            },
            "oLanguage": {
                "sLengthMenu": datatable_parameters.NumberRows + " _MENU_ ",
                "sInfo": datatable_parameters.Display + ' _START_ ' + datatable_parameters.To + ' _END_ ' + datatable_parameters.From + '_TOTAL_ ' + datatable_parameters.Rows,
                "sInfoEmpty": datatable_parameters.Display + ' 0 ' + datatable_parameters.To + ' _END_ ' + datatable_parameters.From + ' _TOTAL_ ' + datatable_parameters.Rows,
                "sZeroRecords": datatable_parameters.NoData,
                "sEmptyTable": datatable_parameters.NoData,
                "oPaginate": {
                    "sPrevious": '<i class="fa fa-angle-double-right"></i>',
                    "sNext": '<i class="fa fa-angle-double-left"></i>'
                }
            },
            "aLengthMenu": [[1, 2, 5, 10, 25, 50, 100], [1, 2, 5, 10, 25, 50, 100]]
        });

        jQuery('.dataTables_length select').select2({ minimumResultsForSearch: -1 });
    });
}

function filterTable(tableName) {
    var oTable = jQuery(tableName).dataTable();
    var oSettings = oTable.fnSettings();
    oSettings.aoPreSearchCols[0].sSearch = jQuery('#name').val();
    oSettings.aoPreSearchCols[1].sSearch = jQuery('#username').val();
    oSettings.aoPreSearchCols[2].sSearch = jQuery('#email').val();
    oTable.fnDraw(false);
}

function clearfilterTable(tableName) {
    var oTable = jQuery(tableName).dataTable();
    var oSettings = oTable.fnSettings();
    for (iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
        oSettings.aoPreSearchCols[iCol].sSearch = '';
    }
    oTable.fnDraw(false);
    jQuery('#name').val("");
    jQuery('#username').val("");
    jQuery('#email').val("");
}
